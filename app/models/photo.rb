class Photo
  attr_accessor :id, :location
  attr_writer :contents

  def initialize(doc=nil)
    if doc
      @id = doc[:_id].to_s
      @location = Point.new(doc[:metadata][:location]) unless doc[:metadata].nil?
      @place = doc[:metadata][:place] unless doc[:metadata].nil?
    end
    @contents=nil
  end
  # convenience method for access to client in console
  def self.mongo_client
    Mongoid::Clients.default
  end

  def  persisted?
    !@id.nil?
  end

  def save
    if persisted?
      description = create_file_info()
      self.class.mongo_client.database.fs.find(:_id => BSON::ObjectId.from_string(@id)).update_one(description)
    else
      file_path = @contents.path
      gps=EXIFR::JPEG.new(file_path).gps
      @location=Point.new(:lng=>gps.longitude, :lat=>gps.latitude)
      #windows HACK [FILE MUST BE OPENED IN RB MODE]
      @contents.close()
      @contents=File.open(file_path, 'rb')
      @contents.rewind
      #END
      description = create_file_info()
      grid_file = Mongo::Grid::File.new(@contents.read, description)
      id=self.class.mongo_client.database.fs.insert_one(grid_file)
      @id = id.to_s
    end
    @id
  end

  def create_file_info()
    description = {}
    description[:content_type]= 'image/jpeg'
    metadata = {}
    metadata[:place]= @place unless @place.nil?
    metadata[:location]=@location.to_hash unless @location.nil?
    metadata[:contentType]='image/jpeg'
    description[:metadata] = metadata
    return description
  end

  def self.all(skip=0, limit=nil)
    mongo_cursor = self.mongo_client.database.fs.find()
    mongo_cursor = mongo_cursor.skip(skip) unless skip.nil?
    mongo_cursor = mongo_cursor.limit(limit) unless limit.nil?
    mongo_cursor.map {|doc| Photo.new(doc)}
  end

  def self.find(id)
    doc = self.mongo_client.database.fs.find(:_id=>BSON::ObjectId.from_string(id)).first
    return doc ? Photo.new(doc) : nil
  end

  def contents
    f = self.class.mongo_client.database.fs.find_one(:_id => BSON::ObjectId.from_string(@id))
    if f
      buffer = ""
      f.chunks.reduce([]) do |x,chunk|
        buffer << chunk.data.data
      end
      return buffer
    end
  end

  def destroy
    self.class.mongo_client.database.fs.find(:_id => BSON::ObjectId.from_string(@id)).delete_one
  end

  def find_nearest_place_id(max_meters)
    doc = Place.near(@location, max_meters).limit(1).projection(:_id=>1).first
    doc ? doc[:_id] : nil
  end

  def place
    @place.nil? ? nil :  Place.find(@place.to_s)
  end

  def place=(place)
    if place.is_a?(Place)
      @place=BSON::ObjectId.from_string(place.id.to_s)
    else
      @place=BSON::ObjectId.from_string(place.to_s)
    end
  end

  def self.find_photos_for_place(place_id)
    self.mongo_client.database.fs.find(:'metadata.place' => BSON::ObjectId.from_string(place_id.to_s))
  end
end