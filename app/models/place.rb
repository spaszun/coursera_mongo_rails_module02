class Place
  include ActiveModel::Model

  attr_accessor :id, :formatted_address, :location, :address_components

  def initialize(params={})
    @id= params[:_id].to_s
    @formatted_address = params[:formatted_address]

    if params[:location]
      @location = Point.new(params[:location])
    else
      if params[:geometry] && params[:geometry][:geolocation]
        @location = Point.new(params[:geometry][:geolocation])
      else
        @location = nil
      end

    end

    if params[:address_components]
      @address_components = params[:address_components].map {|h| AddressComponent.new(h)}
    else
      @address_components = []
    end
  end

  def persisted?
    !@id.nil?
  end

  # def id
  #   BSON::ObjectId.from_string(@id)
  # end

  # convenience method for access to client in console
  def self.mongo_client
    Mongoid::Clients.default
  end

  # convenience method for access to zips collection
  def self.collection
    self.mongo_client['places']
  end

  def self.load_all(file)
    contents = file.read
    hash=JSON.parse(contents)
    places=Place.collection
    places.insert_many(hash)
  end

  def self.find_by_short_name(short_name)
    self.collection.find(:address_components => {:$elemMatch => { :short_name=>short_name}})
  end

  def self.to_places(mongo_cursor)
    places = []
    mongo_cursor.each do |doc|
      if doc
        places << Place.new(doc)
      end
    end
    places
  end

  def self.find(id)
    if id
      doc = self.collection.find(:_id=>BSON::ObjectId.from_string(id)).first
      doc ?  Place.new(doc) : nil
    else
      nil
    end
  end

  def self.all(offset=nil, limit=nil)
    mongo_cursor = self.collection.find()
    mongo_cursor=mongo_cursor.skip(offset) unless offset.nil?
    mongo_cursor=mongo_cursor.limit(limit) unless limit.nil?
    self.to_places(mongo_cursor)
  end

  def destroy
    self.class.collection.find({:_id=>BSON::ObjectId.from_string(@id)}).delete_one
  end

  def self.get_address_components(sort=nil, offset=nil, limit=nil)
    query = []
    query << {:$project=>{ :_id=>1, :address_components=>1, :formatted_address=>1, 'geometry.geolocation'=>1}}
    query << {:$unwind=>'$address_components'}
    query << {:$sort=>sort} unless sort.nil?
    query << {:$skip=>offset} unless offset.nil?
    query << {:$limit=>limit} unless limit.nil?
    self.collection.find.aggregate(query)
  end

  def self.get_country_names
    query = []
    query << {:$project=>{ :_id=>1, :address_components=>1}}
    query << {:$unwind=>'$address_components'}
    query << {:$unwind=>'$address_components.types'}
    query << {:$match=>{'address_components.types'=>'country'}}
    query << {:$group=>{:_id=>'$address_components.long_name'}}
    self.collection.find.aggregate(query).to_a.map {|h| h[:_id]}
  end

  def self.find_ids_by_country_code(country_code)
    query = []
    query << {:$project=>{ :_id=>1, :address_components=>1}}
    #query << {:$unwind=>'$address_components'}
    query << {:$match=>{'address_components.short_name'=>country_code}}
    query << {:$project=>{ :_id=>1}}
    self.collection.find.aggregate(query).map {|doc| doc[:_id].to_s}
  end

  def self.create_indexes
    self.collection.indexes.create_one({ 'geometry.geolocation' => Mongo::Index::GEO2DSPHERE })
  end

  def self.remove_indexes
    self.collection.indexes.drop_all
  end

  def self.near(point, max_meters=nil)
    query_hash = {}
    query_hash[:'$geometry'] = point.to_hash
    query_hash[:'$maxDistance'] = max_meters unless max_meters.nil?
    self.collection.find('geometry.geolocation' => {:'$near'=> query_hash})
  end

  def near(max_meters=nil)
    self.class.to_places(self.class.near(self.location, max_meters))
  end

  def photos(offset=0, limit=nil)
    mongo_cursor = Photo.find_photos_for_place(@id)
    mongo_cursor=mongo_cursor.skip(offset) unless offset.nil?
    mongo_cursor=mongo_cursor.limit(limit) unless limit.nil?
    mongo_cursor.to_a.map {|doc| Photo.new(doc)}
  end

  end