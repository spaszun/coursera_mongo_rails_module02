# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Photo.all.map {|photo| photo.destroy}
Place.all.map {|place| place.destroy}
Place.create_indexes
Place.load_all(File.open('./db/places.json'))
Dir.glob("./db/image*.jpg") { |file_path|
  p = Photo.new
  p.contents = File.open(file_path)
  p.save
  place_id = p.find_nearest_place_id(1*1069.34)
  p.place = place_id ? Place.find(place_id.to_str) : nil
  p.save
}

